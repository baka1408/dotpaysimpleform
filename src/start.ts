﻿import {RouterConfiguration, Router} from 'aurelia-router';
import {autoinject} from 'aurelia-framework';

@autoinject
export class start
{
    router: Router;

    constructor(router: Router)
    {
        this.router = router;
    }

    startPayment() {
        this.router.navigateToRoute('data-form');
    }

    success() {
        this.router.navigate("result?status=OK");
    }

    fail() {
        this.router.navigate("result?status=FAIL");
    }
}