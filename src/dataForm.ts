﻿import {RouterConfiguration, Router, Redirect} from 'aurelia-router';
import {buildQueryString} from 'aurelia-path';
import {autoinject} from 'aurelia-framework';

interface PaymentInfo
{
    productName: string;
    productPrice: number;
    buyerName: string;
}

interface UrlPaymentInfo
{
    id: number;
    amount: string;
    currency: string;
    description: string;
    firstname: string;
    lastname: string;
    email: string;
    URL: string;
    type: number;
    buttontext: string;
}

@autoinject
export class dataForm
{
    enteredInfo: PaymentInfo;
    router: Router;

    constructor(router: Router) {
        this.router = router;
    }

    sendData()
    {
        var params: UrlPaymentInfo = {
            amount: this.enteredInfo.productPrice.toString(),
            currency: 'PLN',
            description: `Test payment from Aurelia application for product : ${this.enteredInfo.productName}`,
            email: this.enteredInfo.buyerName,
            firstname: 'Tester',
            lastname: 'Testowy',
            URL: 'http://localhost:9000/result',
            id: 734510,
            type: 0,
            buttontext: 'Powrót do AureliaTestPayment'
        };
        var urlParams: string = buildQueryString(params);
        var fullUrl = `https://ssl.dotpay.pl/test_payment/?${urlParams}`;

        this.router.navigate(fullUrl);
    }

    leave()
    {
        this.router.navigateBack();
    }

}