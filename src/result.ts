﻿import {RouterConfiguration, Router} from 'aurelia-router';
import {BindingEngine} from 'aurelia-binding';
import {autoinject} from 'aurelia-framework';

@autoinject
export class result {

    message: string;
    status: boolean;
    router: Router;
    engine: BindingEngine;

    constructor(router: Router, engine: BindingEngine) {
        this.router = router;
        this.engine = engine;
        this.status = false;
        this.message = 'No information was received from payment provider. Status will be not updated.';

        let sub = this.engine.propertyObserver(this.router, 'currentInstruction').subscribe(params =>
        {
            debugger;
            if (params.queryParams['status'] === 'OK') {
                this.status = true;
                this.message = 'Payment has been registered successfully!';
            }
            else if (params.queryParams['status'] === 'FAIL')
            {
                this.status = false;
                this.message = 'Payment has been marked as failed. Try again later.';
            }
                
        });
    }

    leave() {
        this.router.navigateToRoute('data-form');
    }
}