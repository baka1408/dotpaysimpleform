import {RouterConfiguration, Router} from 'aurelia-router';

export class App {

    router: Router;
    title: String;

    configureRouter(config: RouterConfiguration, router: Router): void
    {
        this.router = router;
        config.title = 'Payment';
        config.map([
            { route: ['', 'start'], name: 'start', moduleId: 'start' },
            { route: 'form', name: 'data-form', moduleId: 'dataForm' },
            { route: '/result', name: 'result', moduleId: 'result' }]);
    }

    message = 'Payment via DotPay';
}
